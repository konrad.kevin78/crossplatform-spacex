import { GoogleMap } from "@ionic-native/google-maps";

export interface ILocation {
    name: string;
    region: string;
    latitude: number;
    longitude: number;
}

export interface ILaunchPad {
    id: string;
    full_name: string;
    status: string;
    location: ILocation;
    vehicles_launched: string[];
    details: string;
    map: GoogleMap;
}
