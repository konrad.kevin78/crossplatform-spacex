import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {ICapsule} from "../../models/ICapsule";
import {SpaceXProvider} from "../../providers/space-x/space-x";
import {CapsuleDetailsPage} from "../capsule-details/capsule-details";

@IonicPage()
@Component({
  selector: 'page-capsules',
  templateUrl: 'capsules.html',
})
export class CapsulesPage {

  capsules: ICapsule[] = [];

  constructor(private navCtrl: NavController, private spaceXProvider: SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceXProvider.getCapsules().subscribe(data => this.capsules = data);
  }

  doRefresh(refresher) {
    this.spaceXProvider.getCapsules().subscribe(data => {
      this.capsules = data;
      refresher.complete();
    });
  }

  viewDetails(capsule: ICapsule) {
    this.navCtrl.push(CapsuleDetailsPage, capsule).catch(err => console.error(err));
  }
}
