import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { ILaunchPad } from '../../models/ILaunchPad';
import { GoogleMapOptions, GoogleMaps } from "@ionic-native/google-maps";
import {SpaceXProvider} from "../../providers/space-x/space-x";

@IonicPage()
@Component({
  selector: 'page-launchpad-details',
  templateUrl: 'launchpad-details.html',
})
export class LaunchpadDetailsPage {

  launchpad : ILaunchPad;

  constructor(private navParams: NavParams, private spaceXProvider: SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceXProvider.getLaunchpad(this.navParams.data).toPromise()
      .then(this.getLaunchPad)
      .then(this.displayMap)
  }

  ionViewDidLeave() {
    this.launchpad.map.remove().catch(err => console.error(err));
  }

  getLaunchPad = (launchpad : ILaunchPad) => {
    this.launchpad = launchpad;
  };

  displayMap = () => {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.launchpad.location.latitude,
          lng: this.launchpad.location.longitude
        },
        zoom: 6
      }
    };

    this.launchpad.map = GoogleMaps.create('map_details_' + this.launchpad.id, mapOptions);
    this.launchpad.map.addMarker({
      title: this.launchpad.full_name,
      icon: 'blue',
      position: {
        lat: this.launchpad.location.latitude,
        lng: this.launchpad.location.longitude
      }
    }).catch(err => console.error(err));
  };
}
