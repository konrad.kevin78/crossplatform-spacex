import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { SpaceXProvider } from '../../providers/space-x/space-x';
import { ICompanyInfoHistory } from '../../models/ICompanyInfoHistory';

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  events : ICompanyInfoHistory[] = [];

  constructor(private spaceXProvider: SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceXProvider.getCompanyInfoHistory().subscribe(data=> this.events = data);
  }

  doRefresh(refresher) {
    this.spaceXProvider.getCompanyInfoHistory().subscribe(data => {
      this.events = data;
      refresher.complete();
    });
  }

  openURL(url: string) {
    window.open(url, '_blank');
  }
}
