import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { IDetailsCore } from '../../models/IDetailsCore';
import { SpaceXProvider } from '../../providers/space-x/space-x';
import { DetailedCoreDetailsPage } from '../detailed-core-details/detailed-core-details';

@IonicPage()
@Component({
  selector: 'page-detailed-cores',
  templateUrl: 'detailed-cores.html',
})
export class DetailedCoresPage {

  detailsCores : IDetailsCore [] = [];

  constructor(private navCtrl: NavController, private spaceXProvider: SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceXProvider.getDetailsCore().subscribe(data => this.detailsCores = data);
  }

  doRefresh(refresher) {
    this.spaceXProvider.getDetailsCore().subscribe(data => {
      this.detailsCores = data;
      refresher.complete();
    });
  }

  viewDetails(detailsCore : IDetailsCore){
    this.navCtrl.push(DetailedCoreDetailsPage,detailsCore).catch(err => console.error(err));
  }
}
