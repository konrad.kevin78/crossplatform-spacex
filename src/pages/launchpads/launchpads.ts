import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { SpaceXProvider } from '../../providers/space-x/space-x';
import { ILaunchPad } from '../../models/ILaunchPad';
import { LaunchpadDetailsPage } from '../launchpad-details/launchpad-details';

@IonicPage()
@Component({
  selector: 'page-launchpads',
  templateUrl: 'launchpads.html',
})
export class LaunchpadsPage {

  launchpads : ILaunchPad [] = [];

  constructor(private navCtrl: NavController, private spaceXProvider: SpaceXProvider) {}

  ionViewDidEnter() {
    this.spaceXProvider.getLaunchpads().subscribe(data => this.launchpads = data);
  }

  doRefresh(refresher) {
    this.spaceXProvider.getLaunchpads().subscribe(data => {
      this.launchpads = data;
      refresher.complete();
    });
  }

  viewDetails(launchPad : ILaunchPad){
    this.navCtrl.push(LaunchpadDetailsPage, launchPad.id).catch(err => console.error(err));
  }
}
