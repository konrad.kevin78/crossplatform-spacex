import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CapsuleDetailsPage } from './capsule-details';
import { DirectivesModule } from "../../directives/directives.module";

@NgModule({
  declarations: [
    CapsuleDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CapsuleDetailsPage),
    DirectivesModule
  ],
})
export class CapsuleDetailsPageModule {}
