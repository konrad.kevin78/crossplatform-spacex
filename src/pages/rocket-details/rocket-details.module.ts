import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RocketDetailsPage } from './rocket-details';
import {DirectivesModule} from "../../directives/directives.module";

@NgModule({
  declarations: [
    RocketDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(RocketDetailsPage),
    DirectivesModule
  ],
})
export class RocketDetailsPageModule {}
