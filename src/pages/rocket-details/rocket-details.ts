import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import {IRocket} from "../../models/IRocket";
import {SpaceXProvider} from "../../providers/space-x/space-x";

@IonicPage()
@Component({
  selector: 'page-rocket-details',
  templateUrl: 'rocket-details.html',
})
export class RocketDetailsPage {

  rocket: IRocket;

  constructor(private navParams: NavParams, private spaceXProvider : SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceXProvider.getRocket(this.navParams.data).subscribe(rocket => this.rocket = rocket);
  }
}
