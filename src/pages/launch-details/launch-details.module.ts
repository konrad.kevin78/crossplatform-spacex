import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LaunchDetailsPage } from './launch-details';
import { DirectivesModule } from "../../directives/directives.module";

@NgModule({
  declarations: [
    LaunchDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(LaunchDetailsPage),
    DirectivesModule
  ],
})
export class LaunchDetailsPageModule {}
