import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { ICompanyInfo } from '../../models/ICompanyInfo';
import { SpaceXProvider } from '../../providers/space-x/space-x';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  private about : ICompanyInfo;

  constructor(private spaceApi: SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceApi.getCompanyInfo().subscribe(data=> this.about = data);
  }
}
