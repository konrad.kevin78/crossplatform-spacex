import { Component } from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';
import {SpaceXProvider} from "../../../providers/space-x/space-x";
import { ILaunch } from "../../../models/ILaunch";

@IonicPage()
@Component({
  selector: 'page-filters-modal',
  templateUrl: 'filters-modal.html',
})
export class FiltersModalPage {

  yearsOptions: any[] = [{ key: "None", value: "" }];
  rocketsOptions: any[] = [{ rocket_id: "", rocket_name: "None" }];
  sitesOptions: any[] = [{ site_id: "", site_name: "None" }];
  filters: object = {};
  defaultFilters : object = {
    order : "desc",
    launch_year : "",
    launch_success : "",
    site_id : "",
    rocket_id : ""
  };

  constructor(private params: NavParams, private viewCtrl: ViewController, private spaceXProvider: SpaceXProvider) {
    this.filters = params.data;
    this.init();
  }

  init() {
    this.spaceXProvider.getPastLaunches({order : "desc"}).toPromise()
      .then(this.initOptions);
  }

  initOptions = (launches: ILaunch[]) => {
    if (!launches) {
      return;
    }

    let startLimit = Number(launches[0].launch_year);
    let endLimit = Number(launches[launches.length - 1].launch_year);

    for(let i = startLimit; i >= endLimit; i--) {
      this.yearsOptions.push({key: i, value: i});
    }

    launches.forEach(launch => {
      let existingRocket = this.rocketsOptions.filter(rocket => {return rocket.rocket_id == launch.rocket.rocket_id});
      let existingSite = this.sitesOptions.filter(site => {return site.site_id == launch.launch_site.site_id});

      if (existingRocket.length == 0) {
        this.rocketsOptions.push(launch.rocket);
      }

      if (existingSite.length == 0) {
        this.sitesOptions.push(launch.launch_site);
      }
    });

    return launches;
  };

  resetFilters() {
    this.filters = this.defaultFilters;
  }

  dismiss() {
    this.viewCtrl.dismiss(this.filters).catch(err => console.error(err));
  }
}
