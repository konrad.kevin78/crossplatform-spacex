import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {ICompanyInfo} from "../../models/ICompanyInfo";
import {ILaunch} from "../../models/ILaunch";
import {ICapsule} from "../../models/ICapsule";
import {IRocket} from "../../models/IRocket";
import {ILaunchPad} from "../../models/ILaunchPad";
import {IDetailsCapsule} from "../../models/IDetailsCapsule";
import {IDetailsCore} from "../../models/IDetailsCore";
import {ICompanyInfoHistory} from "../../models/ICompanyInfoHistory";


@Injectable()
export class SpaceXProvider {

  private static BASE_URL : String = "https://api.spacexdata.com/v2";

  constructor(public http: HttpClient) { }

  getCompanyInfo() : Observable<ICompanyInfo> {
    return this.http.get<ICompanyInfo>(SpaceXProvider.BASE_URL + "/info");
  }

  getCompanyInfoHistory() : Observable<ICompanyInfoHistory[]> {
    return this.http.get<ICompanyInfoHistory[]>(SpaceXProvider.BASE_URL + "/info/history?order=desc");
  }

  getRockets() : Observable<IRocket[]> {
    return this.http.get<IRocket[]>(SpaceXProvider.BASE_URL + "/rockets");
  }

  getRocket(id : string) : Observable<IRocket> {
    return this.http.get<IRocket>(SpaceXProvider.BASE_URL + "/rockets/" + id);
  }

  getCapsules() : Observable<ICapsule[]> {
    return this.http.get<ICapsule[]>(SpaceXProvider.BASE_URL + "/capsules");
  }

  getLatestLaunch() : Observable<ILaunch> {
    return this.http.get<ILaunch>(SpaceXProvider.BASE_URL + "/launches/latest");
  }

  getNextLaunch() : Observable<ILaunch> {
    return this.http.get<ILaunch>(SpaceXProvider.BASE_URL + "/launches/next");
  }

  getUpcomingLaunches() : Observable<ILaunch[]> {
    return this.http.get<ILaunch[]>(SpaceXProvider.BASE_URL + "/launches/upcoming");
  }

  getPastLaunches(parameters? : any) : Observable<ILaunch[]> {
    let queryString = "";

    if (parameters) {
      Object.keys(parameters).forEach(function (key, index) {
        queryString += index == 0 ? "?" : "&";
        queryString += key + "=" + parameters[key];
      });
    }

    return this.http.get<ILaunch[]>(SpaceXProvider.BASE_URL + "/launches" + queryString);
  }

  getAllLaunches() : Observable<ILaunch[]> {
    return this.http.get<ILaunch[]>(SpaceXProvider.BASE_URL + "/launches/all");
  }

  getLaunchpads() : Observable<ILaunchPad[]> {
    return this.http.get<ILaunchPad[]>(SpaceXProvider.BASE_URL + "/launchpads");
  }

  getLaunchpad(id : string) : Observable<ILaunchPad> {
    return this.http.get<ILaunchPad>(SpaceXProvider.BASE_URL + "/launchpads/" + id);
  }

  getDetailsCapsule() : Observable<IDetailsCapsule[]> {
    return this.http.get<IDetailsCapsule[]>(SpaceXProvider.BASE_URL + "/parts/caps");
  }

  getDetailsCore() : Observable<IDetailsCore[]> {
    return this.http.get<IDetailsCore[]>(SpaceXProvider.BASE_URL + "/parts/cores");
  }
}
